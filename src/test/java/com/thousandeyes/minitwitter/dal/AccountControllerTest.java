package com.thousandeyes.minitwitter.dal;

import com.thousandeyes.minitwitter.controllers.AccountController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * User: arunsudhindra
 * Created on: 6/4/17
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AccountControllerTest {

	@Autowired
	AccountController accountController;

	@Test
	public void getAllUsers() throws Exception {
		List<People> accounts = accountController.getAllUsers();
		assertNotNull(accounts);
		assertEquals(5, accounts.size());
	}

}