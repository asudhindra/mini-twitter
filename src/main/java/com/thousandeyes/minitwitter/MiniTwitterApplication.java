package com.thousandeyes.minitwitter;

import com.thousandeyes.minitwitter.controllers.AccountController;
import com.thousandeyes.minitwitter.dal.People;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@SpringBootApplication
public class MiniTwitterApplication {

	public static void main(String[] args) {
		SpringApplication.run(MiniTwitterApplication.class, args);
	}

	@Configuration
	class AuthenticationConfiguration extends GlobalAuthenticationConfigurerAdapter {

		@Autowired
		AccountController accountController;

		@Override
		public void init(AuthenticationManagerBuilder auth) throws Exception {
			auth.userDetailsService(userDetailsService());
		}

		@Bean
		UserDetailsService userDetailsService() {
			return new UserDetailsService() {
				@Override
				public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
					//TODO: Probably need to replace this call with that of a Repository (time check)
					People account = accountController.findByUsername(username);
					if (account != null) {
						return new User(
								account.getHandle(),
								account.getPassword(),
								true,
								true,
								true,
								true,
								AuthorityUtils.createAuthorityList("USER")
						);
					} else {
						throw new UsernameNotFoundException("Could not find user '" + username + "'");
					}
				}
			};
		}
	}

	@EnableWebSecurity
	@Configuration
	class WebSecurityConfig extends WebSecurityConfigurerAdapter {

		@Override
		protected void configure(HttpSecurity http) throws Exception {
			http.authorizeRequests().anyRequest().fullyAuthenticated().and().
			httpBasic().and().
			csrf().disable();

			http.headers().frameOptions().disable();
		}
	}
}
