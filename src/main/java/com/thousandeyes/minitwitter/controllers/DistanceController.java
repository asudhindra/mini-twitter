package com.thousandeyes.minitwitter.controllers;

import com.thousandeyes.minitwitter.dal.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.*;

/**
 * User: arunsudhindra
 * Created on: 6/7/17
 */
@RestController
@RequestMapping("/")
public class DistanceController {

	@Autowired
	NamedParameterJdbcTemplate jdbcTemplate;

	@RequestMapping(value = "/distance", method = RequestMethod.GET)
	public ResponseEntity findMessages(@RequestParam(value = "to") String destination, Principal principal) {
		Path result = new Path();
		result.setSourceHandle(principal.getName());
		result.setDestHandle(destination);

		//If source and target are the same, shortest path is zero
		if(destination.equalsIgnoreCase(principal.getName())) {
			result.setShortestPath(0);
			return new ResponseEntity(result, HttpStatus.OK);
		}

		//BFS to the rescue!
		Long srcUserId = getUserIdFromHandle(principal.getName());
		Long destUserId = getUserIdFromHandle(destination);

		Queue<Long> queue = new LinkedList<>();
		queue.offer(srcUserId);
		queue.offer(null);//previous user for the src will always be null
		Set<Long> visitedUsers = new HashSet<>();
		//The backRef map will help us compute the shortest length
		Map<Long, Long> backRefs = new HashMap<>();

		while(!queue.isEmpty()) {
			Long user = queue.poll();
			Long previousUser = queue.poll();

			//Visited an already covered user, skip
			if(visitedUsers.contains(user)) {
				continue;
			}

			backRefs.put(user, previousUser);
			//Found the destination, break
			if(user == destUserId) {
				break;
			}
			visitedUsers.add(user);

			for(Long follower : getFollowersForUserId(user)) {
				queue.offer(follower);
				queue.offer(user);
			}

		}
		int count = 0;
		for(Long user = destUserId; user != srcUserId; user = backRefs.get(user)) {
			count++;
		}

		result.setShortestPath(count);
		return new ResponseEntity(result, HttpStatus.OK);

	}

	private Long getUserIdFromHandle(String userHandle) {
		String userIdQuery = "select id from people where handle = :username";
		SqlParameterSource parameterSource = new MapSqlParameterSource("username", userHandle);
		return jdbcTemplate.queryForObject(userIdQuery, parameterSource, Long.class);
	}

	private List<Long> getFollowersForUserId(Long userId) {
		String findFollowersQuery = "select follower_person_id from followers " +
				"where " +
				"person_id = :person_id";
		SqlParameterSource parameterSource = new MapSqlParameterSource("person_id", userId);

		return jdbcTemplate.queryForList(findFollowersQuery, parameterSource, Long.class);
	}
}
