package com.thousandeyes.minitwitter.controllers;

import com.thousandeyes.minitwitter.dal.People;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * User: arunsudhindra
 * Created on: 6/6/17
 */
@Component
public class AccountController {

	@Autowired
	NamedParameterJdbcTemplate jdbcTemplate;

	public List<People> getAllUsers() {
		return jdbcTemplate.query("select * from people", new BeanPropertyRowMapper<>(People.class));
	}

	public People findByUsername(String username) {
		//Error condition check
		if(username == null || username.isEmpty()) {
			return null;
		}

		People result = jdbcTemplate.query("select * from people where handle='" + username + "'",
			new ResultSetExtractor<People>() {
				@Override
				public People extractData(ResultSet rs) throws SQLException, DataAccessException {
					People result = new People();
					if(rs.next()) {
						result.setHandle(rs.getString("handle"));
						result.setPassword(rs.getString("password"));
					}

					return result;
				}
			});
		return result;
	}

}
