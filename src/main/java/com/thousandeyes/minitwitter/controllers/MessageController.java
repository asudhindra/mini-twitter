package com.thousandeyes.minitwitter.controllers;

import com.thousandeyes.minitwitter.dal.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: arunsudhindra
 * Created on: 6/7/17
 */

@RestController
@RequestMapping("/messages")
public class MessageController {

	@Autowired
	NamedParameterJdbcTemplate jdbcTemplate;

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public ResponseEntity findMessages(@RequestParam(value = "search", required = false) String keyword, Principal principal) {

		String userIdQuery = "select id from people where handle = :username";

		//Get the id of the authenticated user
		SqlParameterSource parameterSource = new MapSqlParameterSource("username", principal.getName());
		Long user = jdbcTemplate.queryForObject(userIdQuery, parameterSource, Long.class);

		String followersQuery = "select follower_person_id from followers " +
				"where " +
				"person_id = (select id from people where handle = :username )";

		List<Long> followersList = jdbcTemplate.queryForList(followersQuery, parameterSource, Long.class);

		String queryFilter = (keyword == null || keyword.isEmpty()) ? "" : " and m.content like '" + "%" + keyword + "%'";

		String findMessagesQuery = "select p.handle, m.content from people p, messages m where m.person_id in " +
				"( :userList ) and p.id = m.person_id" + queryFilter;

		Map<String, List<Long>> paramMap = new HashMap<>();
		List<Long> users = new ArrayList<>();
		users.add(user);
		users.addAll(followersList);
		paramMap.put("userList", users);
		parameterSource = new MapSqlParameterSource(paramMap);

		List<Message> messages = new ArrayList<>();
		jdbcTemplate.query(findMessagesQuery, parameterSource, new ResultSetExtractor<List<Message>>() {
			@Override
			public List<Message> extractData(ResultSet rs) throws SQLException, DataAccessException {
				while(rs.next()) {
					Message m = new Message();
					//Evidently, the ResultSetExtractor does not like aliased column names
					m.setHandle(rs.getString(1));
					m.setContent(rs.getString(2));

					messages.add(m);
				}

				return messages;
			}
		});

		return new ResponseEntity(messages, HttpStatus.OK);
	}
}
