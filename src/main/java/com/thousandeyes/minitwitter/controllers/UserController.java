package com.thousandeyes.minitwitter.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: arunsudhindra
 * Created on: 6/6/17
 */

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	NamedParameterJdbcTemplate jdbcTemplate;

	@RequestMapping(value = "/followers", method = RequestMethod.GET)
	public ResponseEntity findFollowers(Principal principal) {
		String query = "select handle from people " +
				"where " +
				"id in (select follower_person_id from followers " +
				"where " +
				"person_id = (select id from people " +
				"where " +
				"handle = :username " +
				")" +
				")";
		SqlParameterSource parameterSource = new MapSqlParameterSource("username", principal.getName());

		List<String> result = jdbcTemplate.queryForList(query, parameterSource, String.class);

		return new ResponseEntity(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/following", method = RequestMethod.GET)
	public ResponseEntity findFollowing(Principal principal) {
		String query = "select handle from people " +
				"where " +
				"id in (select person_id from followers " +
				"where " +
				"follower_person_id = (select id from people " +
				"where " +
				"handle = :username " +
				")" +
				")";
		SqlParameterSource parameterSource = new MapSqlParameterSource("username", principal.getName());

		List<String> result = jdbcTemplate.queryForList(query, parameterSource, String.class);

		return new ResponseEntity(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/follow/{handle}", method = RequestMethod.POST)
	public ResponseEntity addFollower(@PathVariable("handle") String handle, Principal principal) {
		//A user cannot follow him/herself! Report an error as the response
		if(handle.equalsIgnoreCase(principal.getName())) {
			return new ResponseEntity("Invalid request, you cannot follow yourself!", HttpStatus.BAD_REQUEST);
		}

		Long userId = getUserIdFromHandle(principal.getName());
		Long followerPersonId = getUserIdFromHandle(handle);

		//A request to follow a user that is already being followed is an invalid one
		String query = "select follower_person_id from followers where person_id = :userId";
		SqlParameterSource parameterSource = new MapSqlParameterSource("userId", userId);

		//TODO: Checking to see if the user is already followed is an expensive one, need to optimize
		List<Long> result = jdbcTemplate.queryForList(query, parameterSource, Long.class);

		//TODO: Add the contents of the result List to a Set for constant-time lookup, leaving as-is for now
		if(result.contains(followerPersonId)) {
			return new ResponseEntity(
					"Invalid request, you are already following this user!",
					HttpStatus.BAD_REQUEST
			);
		}

		//Now time for a legitimate follow operation
		String insertQuery = "insert into followers(person_id, follower_person_id) values ( :userId , :followerPersonId )";
		Map<String, Long> paramMap = new HashMap<>();
		paramMap.put("userId", userId);
		paramMap.put("followerPersonId", followerPersonId);
		parameterSource = new MapSqlParameterSource(paramMap);

		jdbcTemplate.update(insertQuery, parameterSource);

		//Return a valid response
		return new ResponseEntity(principal.getName() + " is now following " + handle + "!", HttpStatus.OK);
	}

	@RequestMapping(value = "/unfollow/{handle}", method = RequestMethod.DELETE)
	public ResponseEntity unfollow(@PathVariable("handle") String handle, Principal principal) {
		//A user cannot unfollow him/herself! Report an error as the response
		if(handle.equalsIgnoreCase(principal.getName())) {
			return new ResponseEntity("Invalid request, you cannot unfollow yourself!", HttpStatus.BAD_REQUEST);
		}

		Long userId = getUserIdFromHandle(principal.getName());
		Long followerPersonId = getUserIdFromHandle(handle);

		String deleteQuery = "delete from followers where person_id = :userId and follower_person_id = :follower_person_id";
		Map<String, Long> paramMap = new HashMap<>();
		paramMap.put("userId", userId);
		paramMap.put("follower_person_id", followerPersonId);

		//The case where a user tries to follow someone he/she does not already follow is a valid one, for time
		jdbcTemplate.update(deleteQuery, paramMap);

		return new ResponseEntity(handle + " is now unfollowed!", HttpStatus.OK);
	}

	//TODO: Assuming the blue-sky case of crystal clean input values
	private Long getUserIdFromHandle(String userHandle) {
		String userIdQuery = "select id from people where handle = :username";
		SqlParameterSource parameterSource = new MapSqlParameterSource("username", userHandle);
		return jdbcTemplate.queryForObject(userIdQuery, parameterSource, Long.class);
	}
}
