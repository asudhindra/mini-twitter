package com.thousandeyes.minitwitter.dal;

/**
 * User: arunsudhindra
 * Created on: 6/7/17
 */
public class Message {

	String handle;
	String content;

	public String getHandle() {
		return handle;
	}

	public void setHandle(String handle) {
		this.handle = handle;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
