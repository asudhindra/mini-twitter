package com.thousandeyes.minitwitter.dal;


/**
 * User: arunsudhindra
 * Created on: 6/6/17
 */

public class People {

	private Long id;
	private String handle;
	private String password;
	private String name;

	public Long getId() { return id; }

	public String getHandle() { return handle; }

	public String getPassword() { return password; }

	public String getName() { return name; }

	public void setHandle(String handle) {
		this.handle = handle;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) { this.name = name; }
}
