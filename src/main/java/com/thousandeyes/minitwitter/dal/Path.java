package com.thousandeyes.minitwitter.dal;

/**
 * User: arunsudhindra
 * Created on: 6/7/17
 */
public class Path {

	String sourceHandle;
	String destHandle;
	Integer shortestPath;

	public String getSourceHandle() {
		return sourceHandle;
	}

	public void setSourceHandle(String sourceHandle) {
		this.sourceHandle = sourceHandle;
	}

	public String getDestHandle() {
		return destHandle;
	}

	public void setDestHandle(String destHandle) {
		this.destHandle = destHandle;
	}

	public Integer getShortestPath() {
		return shortestPath;
	}

	public void setShortestPath(Integer shortestPath) {
		this.shortestPath = shortestPath;
	}
}
